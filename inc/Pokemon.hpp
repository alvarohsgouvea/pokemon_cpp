#ifndef POKEMON_HPP
#define POKEMON_HPP
#include <iostream>
using namespace std;

class Pokemon{
    private:
        string nickname;
        int species;
        int Hp;
        int Atk;
        int SpAtk;
        int Def;
        int SpDef;
        int Spe;
    public:
        Pokemon();
        ~Pokemon();

        string get_nickname();
        int get_species();
        int get_Hp();
        int get_Atk();
        int get_SpAtk();
        int get_Def();
        int get_SpDef();
        int get_Spe();
        int Current_Hp;

        int take_dmg(int foe_atk, int Def);
        void update_Hp(int foe_atk);
        void set_poke(string nickname, int species);
};

#endif